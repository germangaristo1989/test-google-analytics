import { NgxGoogleAnalyticsModule } from 'ngx-google-analytics';
import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DirectiveComponent } from './directive.component';
import { DirectivaService } from '@casas/rest';
import { DialogComponent } from './dialog/dialog.component';
import { UtilsModule } from '../utils/utils.module';
@NgModule({
  declarations: [DirectiveComponent, DialogComponent],
  imports: [BrowserModule, BrowserAnimationsModule, UtilsModule,NgxGoogleAnalyticsModule],
  providers: [DirectivaService],
  exports: [DirectiveComponent],
  bootstrap: [DirectiveComponent],
})
export class DirectiveModule {}
