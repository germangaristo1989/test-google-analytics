import { environment } from 'src/environments/environment';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidenavModule } from './sidenav/sidenav.module';
import { SidenavRoutingModule } from './sidenav/sidenav-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { UtilsModule } from './utils/utils.module';
import { PlayersModule } from './players/players.module';
import { TournamentModule } from './tournament/tournament.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslationComponent } from './translation/translation.component';
import {
  GoogleAnalyticsService,
  NgxGoogleAnalyticsModule,
  NgxGoogleAnalyticsRouterModule,
} from 'ngx-google-analytics';
import { TeamsModule } from './teams/teams.module';
import { DirectiveModule } from './directive/directive.module';

@NgModule({
  declarations: [AppComponent, TranslationComponent],
  imports: [
    BrowserModule,
    UtilsModule,
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    SidenavModule,
    SidenavRoutingModule,
    PlayersModule,
    TeamsModule,
    DirectiveModule,
    TournamentModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [HttpClient],
      },
    }),
    NgxGoogleAnalyticsModule.forRoot(environment.googleAnalyticsId),
    NgxGoogleAnalyticsRouterModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
