import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { NgxGoogleAnalyticsModule } from 'ngx-google-analytics';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TeamsComponent } from './teams.component';
import { UtilsModule } from '../utils/utils.module';
import { EquiposService, JugadorService } from '@casas/rest';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [TeamsComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    UtilsModule,
    HttpClientModule,
    NgxGoogleAnalyticsModule,
    TranslateModule
  ],
  providers: [EquiposService, JugadorService],
  exports: [TeamsComponent],
  bootstrap: [TeamsComponent],
})
export class TeamsModule {}
