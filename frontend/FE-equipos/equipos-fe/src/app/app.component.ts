import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'equipos-fe';
  constructor(public router: Router, private translate: TranslateService) {
    this.setAppLanguage();
  }

  public setAppLanguage(): void {
    this.translate.setDefaultLang('en');
    this.translate.use(this.translate.getBrowserLang() || '');
  }
}
