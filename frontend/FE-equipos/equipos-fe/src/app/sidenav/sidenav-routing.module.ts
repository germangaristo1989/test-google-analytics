import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayersComponent } from '../players/players.component';
import { TournamentComponent } from '../tournament/tournament.component';
import { TranslationComponent } from '../translation/translation.component';
import { TeamsComponent } from '../teams/teams.component';
import { DirectiveComponent } from '../directive/directive.component';

const routes: Routes = [
  {
    path: 'equipos',
    component: TeamsComponent,
  },
  {
    path: 'jugadores',
    component: PlayersComponent,
  },
  {
    path: 'directiva',
    component: DirectiveComponent,
  },
  {
    path: 'tournament',
    component: TournamentComponent,
  },
  {
    path: 'tranductor',
    component: TranslationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SidenavRoutingModule {}
