import { NgModule } from '@angular/core';

import { SidenavComponent } from './sidenav.component';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterModule } from '@angular/router';
import { SidenavRoutingModule } from './sidenav-routing.module';
import { CommonModule } from '@angular/common';
import { UtilsModule } from '../utils/utils.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
@NgModule({
  declarations: [SidenavComponent],
  imports: [
    UtilsModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    RouterModule,
    SidenavRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  exports: [SidenavComponent],
  bootstrap: [SidenavComponent],
})
export class SidenavModule {}
