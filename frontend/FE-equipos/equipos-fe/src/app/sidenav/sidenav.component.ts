import { Component, HostBinding, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { OverlayContainer } from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})

export class SidenavComponent implements OnInit {
  public activeLang = 'es';
  @HostBinding('class') className = '';
  toggleControl = new FormControl(false);
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private overlay: OverlayContainer,
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang(this.activeLang);
  }
  public cambiarLenguaje(lang: any): void {
    this.activeLang = lang;
    this.translate.use(lang);
  }
  ngOnInit(): void {
    this.toggleControl.valueChanges.subscribe((darkMode) => {
      console.log(darkMode)
      const darkClassName = 'darkMode';
      this.className = darkMode ? darkClassName : 'lightMode';
      if (darkMode) {
        this.overlay.getContainerElement().classList.add(darkClassName);
      } else {
        this.overlay.getContainerElement().classList.remove(darkClassName);
      }
    });
  }
}
