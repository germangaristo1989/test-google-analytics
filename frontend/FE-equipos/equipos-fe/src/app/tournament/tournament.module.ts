import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UtilsModule } from '../utils/utils.module';

import { TournamentComponent } from './tournament.component';
@NgModule({
  declarations: [TournamentComponent],
  imports: [BrowserModule, BrowserAnimationsModule, UtilsModule],
  providers: [],
  exports: [TournamentComponent],
  bootstrap: [TournamentComponent],
})
export class TournamentModule {}
