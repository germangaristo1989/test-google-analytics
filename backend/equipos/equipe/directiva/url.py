from django.urls import include, path

from directiva.views import (DirectiveViewSet as directiveView,
GetDirectivaView as directiveGetView,
DirectiveCreateView as directiveCreateView,
DeleteDirectivaView as directiveDeleteView
                         )

__url_directiva = [
    path('', directiveView.as_view()),
    path('/<int:pk>', directiveGetView.as_view()),
    path('/create', directiveCreateView.as_view()),
    path('/<int:pk>/delete', directiveDeleteView.as_view())
 
]

urlpatterns = [
    path('directiva', include(__url_directiva)),
]
