
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from directiva.models import Directiva
from directiva.serializers import DirectivaSerializer,DirectivaCreateSerializer
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.permissions import AllowAny
# Create your views here.



class DirectiveViewSet(generics.ListAPIView ):
    queryset = Directiva.objects.all()
    serializer_class = DirectivaSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="getAllDirective", operation_description="Obtener lista de Directivos",    tags=['directiva'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class DirectiveCreateView(generics.CreateAPIView):
    queryset = Directiva.objects.all()
    serializer_class = DirectivaCreateSerializer
    permission_classes = [AllowAny]
 
    @swagger_auto_schema(operation_id="createDirective", operation_description="Crear nueva Directiva" , tags=['directiva'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetDirectivaView(APIView):  
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="getDirectiva",
        responses={200: DirectivaSerializer(many=False)},
        tags=['directiva'],
    )
    def get(self, request, pk):
        """
        Return list of Directivas
        """
        try:
            user = Directiva.objects.get(id=pk)
            return Response(DirectivaSerializer(user).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar esa directiva'}, status=HTTP_404_NOT_FOUND)

class DeleteDirectivaView(APIView):      
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="deleteDirectiva",
        serializer_class = DirectivaCreateSerializer,
        tags=['directiva']
    )
    def delete(self, request, pk):
        """
        Delete directiva
        """
        current_directiva = Directiva.objects.get(id=pk)
        current_directiva.delete()      
        return Response({'status': 'delete successfull!'})
