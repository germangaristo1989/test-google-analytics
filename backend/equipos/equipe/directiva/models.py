from cargo.models import Cargo
from django.db import models


# Create your models here.
class Directiva (models.Model): 
    nombre= models.TextField()
    cargo= models.ForeignKey(Cargo,on_delete=models.CASCADE)