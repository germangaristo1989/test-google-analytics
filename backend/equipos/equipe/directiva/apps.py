from django.apps import AppConfig


class DirectivaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'directiva'
