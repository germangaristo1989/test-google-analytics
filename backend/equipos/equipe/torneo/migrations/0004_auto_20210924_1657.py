# Generated by Django 3.2.7 on 2021-09-24 14:57

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('torneo', '0003_alter_torneo_idtipotorneo'),
    ]

    operations = [
        migrations.AddField(
            model_name='torneo',
            name='fecha',
            field=models.DateField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='torneo',
            name='nombre',
            field=models.TextField(default=2),
            preserve_default=False,
        ),
    ]
