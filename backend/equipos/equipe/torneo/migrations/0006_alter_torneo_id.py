# Generated by Django 3.2.7 on 2021-09-30 16:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('torneo', '0005_rename_idtipotorneo_torneo_tipotorneo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torneo',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
