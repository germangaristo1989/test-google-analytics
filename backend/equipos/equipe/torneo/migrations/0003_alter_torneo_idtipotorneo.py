# Generated by Django 3.2.7 on 2021-09-24 14:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tipoTorneo', '0002_rename_logros_tipotorneo'),
        ('torneo', '0002_alter_torneo_idtipotorneo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torneo',
            name='idtipotorneo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tipoTorneo.tipotorneo'),
        ),
    ]
