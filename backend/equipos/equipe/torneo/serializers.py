from rest_framework.relations import PrimaryKeyRelatedField
from tipoTorneo.models import TipoTorneo
from tipoTorneo.serializers import TipoTorneoSerializer
from torneo.models import Torneo
from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import  ModelSerializer



class TorneoCreateSerializer(ModelSerializer):
    
    id = ReadOnlyField()
    tipotorneo=PrimaryKeyRelatedField(queryset = TipoTorneo.objects.all())
    class Meta:
        ref_name = 'torneoCreate'
        model= Torneo
        fields=['id','tipotorneo','nombre','fecha']
  
class TorneoSerializer(ModelSerializer):
    tipotorneo= TipoTorneoSerializer(many=False, read_only=True)
    class Meta:
        ref_name = 'Torneo'
        model= Torneo
        fields=['id','tipotorneo','nombre','fecha']