from tipoTorneo.models import TipoTorneo
from django.db import models

# Create your models here.
class Torneo (models.Model): 
    tipotorneo=models.ForeignKey(TipoTorneo, on_delete=models.CASCADE)
    nombre=models.TextField()
    fecha= models.DateField()
    