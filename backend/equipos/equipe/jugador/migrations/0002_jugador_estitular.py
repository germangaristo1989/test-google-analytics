# Generated by Django 3.2.7 on 2021-09-28 16:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jugador', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='jugador',
            name='esTitular',
            field=models.BooleanField(default=True),
            preserve_default=False,
        ),
    ]
