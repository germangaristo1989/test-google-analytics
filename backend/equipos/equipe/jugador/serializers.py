from jugador.models import Jugador
from rest_framework import serializers



class JugadorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model= Jugador
        fields=['id','nombre','posicion','esMito','esTitular']