from django.urls import include, path

from entrenador.views import (EntrenadorViewSet as entrenadorView,
                          UpdateEntrenadorView as entrenadorUpdateView, GetEntrenadorView as entrenadorGetVew,DeleteEntrenadorView as entrenadorDeleteView)

__url_entrenador = [
    path('', entrenadorView.as_view()),
    path('/<int:pk>', entrenadorGetVew.as_view()),
    path('/<int:pk>/', entrenadorUpdateView.as_view()),
    path('/<int:pk>/delete', entrenadorDeleteView.as_view()),
 
]

urlpatterns = [
    path('entrenador', include(__url_entrenador)),
]
