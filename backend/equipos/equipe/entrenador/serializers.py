from entrenador.models import Entrenador
from rest_framework import serializers
from rest_framework.fields import CharField,ReadOnlyField
from rest_framework.serializers import  Serializer

class EntrenadorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model= Entrenador
        fields=['nombre']


class UpdateEntrenadorSerializer(Serializer):
    """
    User update entrenador
    """
    id = ReadOnlyField()
    nombre = CharField(
        max_length=None, min_length=None, allow_blank=False, trim_whitespace=True)