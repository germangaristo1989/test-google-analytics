from rest_framework import permissions
from entrenador.serializers import EntrenadorSerializer,UpdateEntrenadorSerializer
from entrenador.models import Entrenador
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND

class EntrenadorViewSet(generics.ListAPIView, generics.CreateAPIView):
    queryset = Entrenador.objects.all()
    serializer_class = EntrenadorSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(operation_id="getAllEntrenador", operation_description="Obtener lista de entrenadores",
        tags=['entrenador'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    @swagger_auto_schema(operation_id="createEntrenador", operation_description="Crear nuevo Entrenador",
        tags=['entrenador'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetEntrenadorView(APIView):  
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_id="getEntrenador",
        responses={200: EntrenadorSerializer(many=True)},
        tags=['entrenador'],
    )
    def get(self, request, pk):
        """
        Return list of Entrenador
        """
        try:
            user = Entrenador.objects.get(id=pk)
            return Response(EntrenadorSerializer(user).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese entrenador'}, status=HTTP_404_NOT_FOUND)

class UpdateEntrenadorView(APIView):
    permission_classes = [AllowAny]

    @ swagger_auto_schema(
        operation_id="updateEntrenador",
        request_body=UpdateEntrenadorSerializer,
        responses={200: UpdateEntrenadorSerializer(many=True)},
        tags=['entrenador'],
    )
    def put(self, request, pk):
        """
        Update Entrenador
        """
        # get current entrenador
        current_entrenador = Entrenador.objects.get(
            id=pk)

        new_name = request.data.pop('nombre')
        
        current_entrenador.nombre = new_name
        current_entrenador.save()
        #if new is main change others as not main
        data = {
            "nombre": current_entrenador.nombre
        }
        # update phone with new data
        return Response(
            UpdateEntrenadorSerializer(data, many=False).data)


class DeleteEntrenadorView(APIView):      
    permission_classes = [AllowAny] 
    @swagger_auto_schema(
        operation_id="deleteEntrenador",
        tags=['entrenador'],
    )
    def delete(self, request, pk):
        """
        Delete Entrenador
        """
        current_entrenador = Entrenador.objects.get(id=pk)
        current_entrenador.delete()      
        return Response({'status': 'delete successfull!'})
