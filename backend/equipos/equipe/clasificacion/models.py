from django.db import models
from equipo.models import Equipo
from torneo.models import Torneo
# Create your models here.
class Clasificacion (models.Model): 
    idEquipo=models.ForeignKey(Equipo,on_delete=models.CASCADE)
    posicion= models.IntegerField()
    goles= models.IntegerField()
    idtorneo=models.ForeignKey(Torneo, on_delete=models.CASCADE)