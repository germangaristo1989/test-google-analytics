from rest_framework.relations import PrimaryKeyRelatedField
from clasificacion.models import Clasificacion
from equipo.models import Equipo
from equipo.serializers import EquipoSerializer
from torneo.serializers import TorneoSerializer
from torneo.models import Torneo
from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import  ModelSerializer



class ClasificacionCreateSerializer(ModelSerializer):
    
    id = ReadOnlyField()
    idEquipo=PrimaryKeyRelatedField(queryset = Equipo.objects.all())
    idtorneo=PrimaryKeyRelatedField(queryset = Torneo.objects.all())
    class Meta:
        ref_name = 'clasificacionCreate'
        model= Clasificacion
        fields=['id','idEquipo','posicion','goles','idtorneo']
  
class ClasificacionSerializer(ModelSerializer):
    idEquipo= EquipoSerializer(many=False, read_only=True)
    idtorneo= TorneoSerializer(many=False, read_only=True)
   
    class Meta:
        ref_name = 'clasificacion'
        model= Clasificacion
        fields=['id','idEquipo','posicion','goles','idtorneo']