from django.urls import include, path

from tipoTorneo.views import (TipoTorneoViewSet as tipoTorneoView,
                          UpdateTipoTorneoView as tipoTorneoUpdateView,
                           GetTipoTorneoView as tipoTorneoGetVew,
                           DeleteTipoTorneoView as tipoTorneoDeleteView)

__urls_tipo_torneo = [
    path('', tipoTorneoView.as_view()),
    path('/<int:pk>', tipoTorneoUpdateView.as_view()),
    path('/<int:pk>/', tipoTorneoGetVew.as_view()),
    path('/<int:pk>/delete', tipoTorneoDeleteView.as_view()),
 
]

urlpatterns = [
    path('tipo', include(__urls_tipo_torneo)),
]
