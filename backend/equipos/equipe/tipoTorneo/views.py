from rest_framework import permissions
from tipoTorneo.serializers import TipoTorneoSerializer,UpdateTipoTorneoSerializer
from tipoTorneo.models import TipoTorneo
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND

class TipoTorneoViewSet(generics.ListAPIView, generics.CreateAPIView):
    queryset = TipoTorneo.objects.all()
    serializer_class = TipoTorneoSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(operation_id="getAllTipoTorneo", operation_description="Obtener lista de Tipos de torneo",
        tags=['tipotorneo'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    @swagger_auto_schema(operation_id="createTipoTorneo", operation_description="Crear nuevo Tipo de torneo",
        tags=['tipotorneo'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetTipoTorneoView(APIView):  
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_id="getTipoTorneo",
        responses={200: TipoTorneoSerializer(many=True)},
        tags=['tipotorneo'],
    )
    def get(self, request, pk):
        """
        Return list of TipoTorneo
        """
        try:
            user = TipoTorneo.objects.get(id=pk)
            return Response(TipoTorneoSerializer(user).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese current_tipo_torneo'}, status=HTTP_404_NOT_FOUND)

class UpdateTipoTorneoView(APIView):
    permission_classes = [AllowAny]

    @ swagger_auto_schema(
        operation_id="updateTipoTorneo",
        request_body=UpdateTipoTorneoSerializer,
        responses={200: UpdateTipoTorneoSerializer(many=True)},
        tags=['tipotorneo'],
    )
    def put(self, request, pk):
        """
        Update Tipo torneo
        """
        # get current Tipo Torneo
        current_tipo_torneo = TipoTorneo.objects.get(
            id=pk)

        new_name = request.data.pop('tipo_torneo')
        
        current_tipo_torneo.tipo_torneo = new_name
        current_tipo_torneo.save()
        #if new is main change others as not main
        data = {
            "tipo_torneo": current_tipo_torneo.tipo_torneo
        }
        # update phone with new data
        return Response(
            UpdateTipoTorneoSerializer(data, many=False).data)


class DeleteTipoTorneoView(APIView):      
    permission_classes = [AllowAny] 
    @swagger_auto_schema(
        operation_id="deleteTipoTorneo",
        tags=['tipotorneo'],
    )
    def delete(self, request, pk):
        """
        Delete torneo
        """
        current_torneo = TipoTorneo.objects.get(id=pk)
        current_torneo.delete()      
        return Response({'status': 'delete successfull!'})
