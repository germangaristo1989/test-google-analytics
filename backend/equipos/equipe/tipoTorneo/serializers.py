from tipoTorneo.models import TipoTorneo
from rest_framework.fields import CharField,ReadOnlyField
from rest_framework.serializers import  Serializer
from rest_framework import serializers

class TipoTorneoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model= TipoTorneo
        fields=['id','tipo_torneo']

class UpdateTipoTorneoSerializer(Serializer):
    
    id = ReadOnlyField()
    tipo_torneo = CharField(
        max_length=None, min_length=None, allow_blank=False, trim_whitespace=True)
  