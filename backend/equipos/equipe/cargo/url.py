from django.urls import include, path

from cargo.views import ( 
    CargoViewSet as cargoView,
    UpdateCargoView as updateCargoView,
    DeleteCargoView as deleteCargoView,
    GetCargoView as cargoGetView,
    geeks_view as template
                        )

__url_cargo = [
    path('/template', template),
    path('', cargoView.as_view()),
    path('/<int:pk>', cargoGetView.as_view()),
    path('/<int:pk>/', updateCargoView.as_view()),
    path('/<int:pk>/delete', deleteCargoView.as_view()),
 
]

urlpatterns = [
    path('cargo', include(__url_cargo)),
]
