from django.http.response import HttpResponse
from cargo.serializer import CargoSerializer,UpdateCargoSerializer
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework import permissions
from cargo.models import Cargo
# import Http Response from django
from django.shortcuts import render

# create a function
def geeks_view(request):
    # create a dictionary to pass
    # data to the template
    context ={
        "data":CargoSerializer(Cargo.objects.get(id=2)).data,
        "list":CargoSerializer(Cargo.objects.filter(), many=True).data
    }
    # return response with template and context
    return render(request, "index.html", context)




class CargoViewSet(generics.ListAPIView, generics.CreateAPIView):
    queryset = Cargo.objects.all()
    serializer_class = CargoSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(operation_id="getAllCargo", operation_description="Obtener lista de Cargos", tags=['Cargo'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    @swagger_auto_schema(operation_id="createCargo", operation_description="Crear nuevo Cargo",tags=['Cargo'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetCargoView(APIView):  
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_id="getCargo",
        responses={200: CargoSerializer(many=True)},
        tags=['Cargo'],
    )
    def get(self, request, pk):
        """
        Return list of Cargos
        """
        try:
            cargo = Cargo.objects.get(id=pk)
            return Response(CargoSerializer(cargo).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese cargo'}, status=HTTP_404_NOT_FOUND)


class UpdateCargoView(APIView):
    permission_classes = [AllowAny]

    @ swagger_auto_schema(
        operation_id="updateCargo",
        request_body=UpdateCargoSerializer,
        responses={200: UpdateCargoSerializer(many=True)},
        tags=['Cargo'],
    )
    def put(self, request, pk):
        """
        Update Cargo
        """
        # get current Cargo
        current_cargo = Cargo.objects.get(
            id=pk)

        new_cargo = request.data.pop('cargo')
        
        current_cargo.cargo = new_cargo
        current_cargo.save()
        data = {
            "cargo": current_cargo.cargo
        }
        # update cargo with new data
        return Response(
            UpdateCargoSerializer(data, many=False).data)


class DeleteCargoView(APIView):      
    permission_classes = [AllowAny] 
    @swagger_auto_schema(
        operation_id="deleteCargo",
        tags=['Cargo'],
    )
    def delete(self, request, pk):
        """
        Delete Cargo
        """
        current_cargo = Cargo.objects.get(id=pk)
        current_cargo.delete()      
        return Response({'status': 'delete successfull!'})
