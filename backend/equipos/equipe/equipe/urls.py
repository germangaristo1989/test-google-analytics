"""equipe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.urls import include, path
from django.conf.urls import url, include
from django.contrib import admin
from jugador import views as JugadorViews
from rest_framework import routers
from entrenador.url import urlpatterns as urls_entrenador
from cargo.url import urlpatterns as urls_cargo
from directiva.url import urlpatterns as urls_directiva
from tipoTorneo.url import urlpatterns as urls_tipo_torneo
from torneo.url import urlpatterns as urls_torneo
from premios.url import urlpatterns as urls_premios
from logros.url import urlpatterns as urls_logros
from equipo.url import urlpatterns as urls_equipo
from clasificacion.url import urlpatterns as urls_clasificacion

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()

router.register(r'jugador', JugadorViews.JugadorViewSet)



current_version = 'v1'
module_name = 'equipos'
base_url = 'api/' + current_version + '/' + module_name + '/'

schema_view = get_schema_view(
   openapi.Info(
      title="equipos API",
      default_version=current_version,
      description="Equipos REST API definition",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('admin/', admin.site.urls),
    path(base_url, include(urls_entrenador)),
    path(base_url, include(urls_cargo)),    
    path(base_url, include(urls_directiva)),
    path(base_url, include(urls_tipo_torneo)),    
    path(base_url, include(urls_torneo)),
    path(base_url, include(urls_premios)),
    path(base_url, include(urls_logros)),        
    path(base_url, include(urls_equipo)), 
    path(base_url, include(urls_clasificacion)),        

    path(base_url, include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
