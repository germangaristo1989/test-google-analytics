from django.urls import include, path

from equipo.views import (EquipoViewSet as equipoView,
EquiposCreateView as equipoCreateView,
GetEquipoView as equipoGetView,
DeleteEquipoView as equipoDeleteView,
TitularesView as titularesView,
SuplentesView as suplentesView,
CambioTitularView as cambioTitularView
                         )

__url_equipo = [
    path('', equipoView.as_view()),
    # path('/<int:pk>/update/<int:jugador_id>', equipoUpdateView.as_view()),
    path('/create', equipoCreateView.as_view()),
    path('/<int:pk>', equipoGetView.as_view()),
    path('/<int:pk>/delete', equipoDeleteView.as_view()),
    path('/<int:pk_equipo>/titulares', titularesView.as_view()),
    path('/<int:pk_equipo>/suplentes', suplentesView.as_view()),
    path('/<int:pk_equipo>/<int:titular_actual>/<int:nuevo_titular>/', cambioTitularView.as_view()),
    
 
]

urlpatterns = [
    path('equipo', include(__url_equipo)),
]
