from rest_framework.relations import PrimaryKeyRelatedField
from logros.models import Logros
from jugador.models import Jugador
from equipo.models import Equipo
from entrenador.models import Entrenador
from directiva.models import Directiva
from entrenador.serializers import EntrenadorSerializer
from logros.serializers import LogrosSerializer
from directiva.serializers import DirectivaSerializer
from jugador.serializers import JugadorSerializer
from rest_framework.fields import CharField,ReadOnlyField
from rest_framework.serializers import  ModelSerializer


class EquipoCreateSerializer(ModelSerializer):

    id = ReadOnlyField()
    jugador=PrimaryKeyRelatedField(queryset = Jugador.objects.all())
    entrenador=PrimaryKeyRelatedField(queryset = Entrenador.objects.all())
    logros=PrimaryKeyRelatedField(queryset = Logros.objects.all())
    directiva=PrimaryKeyRelatedField(queryset = Directiva.objects.all())
    class Meta:
        ref_name = 'equipocreate'
        model= Equipo
        fields=['id','nombre','jugador','entrenador','logros','directiva']


class EquipoSerializer(ModelSerializer):
    jugador= JugadorSerializer(many=True, read_only=True)
    entrenador= EntrenadorSerializer(many=False, read_only=True)
    logros= LogrosSerializer(many=False, read_only=True)
    directiva= DirectivaSerializer(many=False, read_only=True)
    class Meta:
        ref_name = 'Equipo'
        model= Equipo
        fields=['id','nombre','jugador','entrenador','logros','directiva']

class UpdateEquipoSerializer(ModelSerializer):
    """
    User update entrenador
    """
    id = ReadOnlyField()
    nombre = CharField(
        max_length=None, min_length=None, allow_blank=False, trim_whitespace=True)
    jugador= JugadorSerializer(many=False, read_only=True)
    entrenador= EntrenadorSerializer(many=False, read_only=True)
    logros= LogrosSerializer(many=False, read_only=True)
    directiva= DirectivaSerializer(many=False, read_only=True)
    class Meta:
        ref_name = 'equipo'
        model= Equipo
        fields=['id','nombre','jugador','entrenador','logros','directiva']