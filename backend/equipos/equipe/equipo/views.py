from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from drf_yasg.utils import swagger_auto_schema
from jugador.models import Jugador
from equipo.models import Equipo
from jugador.serializers import JugadorSerializer
from equipo.serializers import EquipoSerializer, EquipoCreateSerializer,UpdateEquipoSerializer
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework import generics
from rest_framework import permissions

class EquipoViewSet(generics.ListAPIView ):
    queryset = Equipo.objects.all()
    serializer_class = EquipoSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(operation_id="getAllEquipo", operation_description="Obtener lista de Equipos",    tags=['equipos'])
    def get(self, request, *args, **kwargs):
        """
        Return list of torneo
        """
        try:
            equipo: Equipo =Equipo.objects.all()
            jugador= {}
            data={
                'nombre': equipo[0].nombre,
                'jugador':[],
                'entrenador':equipo[0].entrenador,
                'logros':equipo[0].logros,
                'directiva':equipo[0].directiva
            }
            return Response(EquipoSerializer(data,many= False).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese Equipo'}, status=HTTP_404_NOT_FOUND)

class EquiposCreateView(generics.CreateAPIView):
    queryset = Equipo.objects.all()
    serializer_class = EquipoCreateSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(operation_id="createEquipo", operation_description="Crear nuevo Equipo" , tags=['equipos'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetEquipoView(APIView):
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="getEquipo",
        responses={200: EquipoSerializer(many=True)},
        tags=['equipos'],
    )
    def get(self, request, pk):
        """
        Return list of torneo
        """
        try:
            equipo: Equipo = Equipo.objects.get(id=pk)
            jugador= equipo.getTitulares()
            data={
                'nombre': equipo.nombre,
                'jugador':jugador,
                'entrenador':equipo.entrenador,
                'logros':equipo.logros,
                'directiva':equipo.directiva
            }
            return Response(EquipoSerializer(data,many= False).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese Equipo'}, status=HTTP_404_NOT_FOUND)


class DeleteEquipoView(APIView):
    permission_classes = [AllowAny]
    @swagger_auto_schema(
        operation_id="deleteEquipo",
        serializer_class = EquipoCreateSerializer,
        tags=['equipos']
    )
    def delete(self, request, pk):
        """
        Delete equipo
        """
        current_torneo = Equipo.objects.get(id=pk)
        current_torneo.delete()
        return Response({'status': 'delete successfull!'})


class TitularesView(APIView):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_id="getTitulares",
        responses={200: JugadorSerializer(many=True)},
        tags=['equipos'],
    )
    def get(self, request, pk_equipo):
        """
        Return list of Jugadores Titulares
        """
        try:

            equipo = Equipo.objects.get(id=pk_equipo)
            titulares = equipo.getTitulares()
            return Response(JugadorSerializer(titulares,many=True).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese jugador'}, status=HTTP_404_NOT_FOUND)

class SuplentesView(APIView):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_id="getSuplentes",
        responses={200: JugadorSerializer(many=True)},
        tags=['equipos'],
    )
    def get(self, request, pk_equipo):
        """
        Return list of Jugadores Titulares
        """
        try:

            equipo = Equipo.objects.get(id=pk_equipo)
            titulares = equipo.getSuplentes()
            return Response(JugadorSerializer(titulares,many=True).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese jugador'}, status=HTTP_404_NOT_FOUND)


class CambioTitularView(APIView):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_id="cambioTitular",
        responses={200: JugadorSerializer(many=True)},
        tags=['equipos'],
    )
    def put(self, request, pk_equipo, titular_actual, nuevo_titular):
        """
        Return list of Jugadores Titulares
        """
        try:
            equipo = Equipo.objects.get(id=pk_equipo)
            equipo.cambioDeTitular(titular_actual,nuevo_titular)

            titulares= equipo.getTitulares()
            equipo.jugador= Jugador(nuevo_titular);
            equipo.save()
            UpdateEquipoSerializer(equipo, many=False).data
            return Response(JugadorSerializer(titulares,many=True).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese jugador'}, status=HTTP_404_NOT_FOUND)