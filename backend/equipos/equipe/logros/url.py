from django.urls import include, path

from logros.views import (LogrosViewSet as logrosView,
GetLogrosView as logrosGetView,
LogrosCreateView as logrosCreateView,
DeleteLogrosView as logrosDeleteView
                         )

__url_logros = [
    path('', logrosView.as_view()),
    path('/<int:pk>', logrosGetView.as_view()),
    path('/create', logrosCreateView.as_view()),
    path('/<int:pk>/delete', logrosDeleteView.as_view())
 
]

urlpatterns = [
    path('logros', include(__url_logros)),
]
