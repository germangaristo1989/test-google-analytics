from rest_framework.relations import PrimaryKeyRelatedField
from logros.models import Logros
from premios.models import Premios
from torneo.models import Torneo
from torneo.serializers import TorneoSerializer
from premios.serializers import PremiosSerializer
from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import  ModelSerializer


class LogrosCreateSerializer(ModelSerializer):
    
    id = ReadOnlyField()
    torneos=PrimaryKeyRelatedField(queryset = Torneo.objects.all())
    premio=PrimaryKeyRelatedField(queryset = Premios.objects.all())
    class Meta:
        ref_name = 'logroscreate'
        model= Logros
        fields=['id','torneos','premio']
  

class LogrosSerializer(ModelSerializer):
    torneos= TorneoSerializer(many=False, read_only=True)
    premio= PremiosSerializer(many=False, read_only=True)
    class Meta:
        ref_name = 'Logros'
        model= Logros
        fields=['id','torneos','premio']

