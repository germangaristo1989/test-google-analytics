from premios.models import Premios
from torneo.models import Torneo
from django.db import models

# Create your models here.
class Logros(models.Model):
    torneos= models.ForeignKey(Torneo, on_delete=models.CASCADE)
    premio= models.ForeignKey(Premios, on_delete=models.CASCADE)