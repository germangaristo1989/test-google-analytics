from premios.models import Premios
from rest_framework.fields import CharField,DateField, IntegerField, ReadOnlyField
from rest_framework.serializers import  Serializer
from rest_framework import serializers

class PremiosSerializer(serializers.HyperlinkedModelSerializer):
 
    class Meta:
        model= Premios
        fields=['id','nombre','fecha','posicion']

class UpdatePremiosSerializer(Serializer):
    """
    User update Premios
    """
    id = ReadOnlyField()
    nombre = CharField(
        max_length=None, min_length=None, allow_blank=False, trim_whitespace=True)
    fecha= DateField()
    posicion= IntegerField()
