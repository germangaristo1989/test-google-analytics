from rest_framework import permissions
from premios.serializers import PremiosSerializer,UpdatePremiosSerializer
from premios.models import Premios
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import HTTP_404_NOT_FOUND

class PremiosViewSet(generics.ListAPIView, generics.CreateAPIView):
    queryset = Premios.objects.all()
    serializer_class = PremiosSerializer
    permission_classes = [AllowAny]

    @swagger_auto_schema(operation_id="getAllPremios", operation_description="Obtener lista de Premios",
        tags=['premios'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    @swagger_auto_schema(operation_id="createPremios", operation_description="Crear nuevo Premio",
        tags=['premios'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

class GetPremiosView(APIView):  
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_id="getPremios",
        responses={200: PremiosSerializer(many=True)},
        tags=['premios'],
    )
    def get(self, request, pk):
        """
        Return list of Premios
        """
        try:
            user = Premios.objects.get(id=pk)
            return Response(PremiosSerializer(user).data)
        except ObjectDoesNotExist:
            return Response({'status': 'No se puede encontrar ese Premio'}, status=HTTP_404_NOT_FOUND)

class UpdatePremiosView(APIView):
    permission_classes = [AllowAny]

    @ swagger_auto_schema(
        operation_id="updatePremios",
        request_body=UpdatePremiosSerializer,
        responses={200: UpdatePremiosSerializer(many=True)},
        tags=['premios'],
    )
    def put(self, request, pk):
        """
        Update Premios
        """
        # get current Premios
        current_Premios = Premios.objects.get(
            id=pk)

        new_name = request.data.pop('nombre')
        new_date = request.data.pop('fecha')
        new_position = request.data.pop('posicion')
        current_Premios.nombre = new_name
        current_Premios.fecha = new_date
        current_Premios.posicion = new_position

        current_Premios.save()
        #if new is main change others as not main
        data = {
            "nombre": current_Premios.nombre,
            "fecha": current_Premios.fecha,
            "posicion": current_Premios.posicion
        }
        # update phone with new data
        return Response(
            UpdatePremiosSerializer(data, many=False).data)


class DeletePremiosView(APIView):      
    permission_classes = [AllowAny] 
    @swagger_auto_schema(
        operation_id="deletePremios",
        tags=['premios'],
    )
    def delete(self, request, pk):
        """
        Delete Premios
        """
        current_Premios = Premios.objects.get(id=pk)
        current_Premios.delete()      
        return Response({'status': 'delete successfull!'})
