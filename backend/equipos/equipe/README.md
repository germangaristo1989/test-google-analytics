Equipos Be
===

## 🚀 start project
First step to start project
```
 django-admin startproject equipos
```
```
 python -m venv venv
```
```
 venv\Scripts\activate
```
```
 pip install Django
```

## ✂️ shortcuts
Shortcuts utils for django applications
```
 pip freeze > requirements.txt
```
```
 pip install -r requirements.txt
```
```
 python manage.py runserver
```
```
 python manage.py makemigrations
```
```
 python manage.py makemigrations --name [your-name] --empty [your-app]
```
```
 python manage.py migrate
```
```
 python manage.py startapp [your-app-name]
```

## 💿 sqlite3
run project with local settings and sqlite3 (without docker image)
```
 python manage.py runserver --settings agube.settings-local
```
```
 python manage.py migrate --settings agube.settings-local
```


## ✅ tests
execute tests with coverage statistics
```
 pytest --cov --cov-report=html
```
